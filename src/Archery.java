import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Archery {
	private static int userInput;
	private static int wind;
	private static int direction;
	private static int speed;
	private static int count;
	private static boolean correct;
	private static Random number;
	private static BufferedReader in;
	private static int point;

	public static void staticShooting() throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Dein Ziel ist es, ein unbewegtes Ziel zu treffen.\n");
		number = new Random();
		wind = number.nextInt(4) + 1;
		switch(wind){
			case 1: System.out.print("Der Wind kommt von links und ist stark.\n");
				break;
			case 2: System.out.print("Der Wind kommt von links und ist schwach.\n");
				break;
			case 3: System.out.print("Es ist windstill.\n");
				break;
			case 4: System.out.print("Der Wind kommt von rechts und ist schwach.\n");
				break;
			case 5: System.out.print("Der Wind kommt von rechts und ist stark.\n");
				break;
			default: System.out.print("Es ist windstill.\n");
				wind = 3;
				break;
		}
		correct = false;
		while(!correct){
			while(!correct){
				System.out.print("Gib eine Zahl zwischen 1 und 11 ein, mit 1 schießt du weit nach links, mit 5 geradeaus und mit 11 weit nach links.\n");
				userInput = Integer.parseInt(in.readLine());
				if((userInput < 12) && (userInput > 0)){
					correct = true;
				} else {
					System.out.print("Falsche Eingabe!\n");
				}
			}
			correct = false;
			count++;
			if((wind == 1) && (wind + userInput < 4)){
				System.out.print("Treffer!\n");
				correct = true;
			} else if ((wind == 2) && (wind + userInput < 7) && (wind + userInput > 4)){
				System.out.print("Treffer!\n");
				correct = true;
			} else if ((wind == 3) && (userInput == 5)){
				System.out.print("Treffer!\n");
				correct = true;
			} else if ((wind == 4) && (wind + userInput < 13) && (wind + userInput > 9)){
				System.out.print("Treffer!\n");
				correct = true;
			} else if ((wind == 5) && (wind + userInput < 17) && (wind + userInput > 14)){
				System.out.print("Treffer!\n");
				correct = true;
			} else {
				System.out.print("Du hast das Ziel leider verfehlt.");
			}
		}
		if(count == 1){
			System.out.print("Sehr gut, du hast gleich beim ersten Schuss getroffen!\n");
			return;
		} else if (count < 4){
			System.out.print("Nicht schlecht! Du hast " + count + " Versuche gebraucht.\n");
			return;
		} else if (count < 7){
			System.out.print("Das geht aber besser. Möchtest Du ein wenig trainieren? 1 für Ja oder 2 für Nein\n");
			userInput = Integer.parseInt(in.readLine());
			if(userInput == 1){
				staticTraining();
			}
			return;
		} else {
			System.out.print("Du musst definitiv mehr trainieren!\n");
			staticTraining();
		}
	}

	public static void moving() throws IOException {

	}

	public static void staticTraining() throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Willkommen zum Training. Hier kannst Du üben, bis Du ein unbewegtes Ziel gut treffen kannst. Ziel ist es mindestens 3 mal zu treffen. Viel Spaß!\n");
		point = 0;
		while(point < 4){
			number = new Random();
			wind = number.nextInt(4) + 1;
			switch(wind){
				case 1: System.out.print("Der Wind kommt von links und ist stark.\n");
					break;
				case 2: System.out.print("Der Wind kommt von links und ist schwach.\n");
					break;
				case 3: System.out.print("Es ist windstill.\n");
					break;
				case 4: System.out.print("Der Wind kommt von rechts und ist schwach.\n");
					break;
				case 5: System.out.print("Der Wind kommt von rechts und ist stark.\n");
					break;
				default: System.out.print("Es ist windstill.\n");
					wind = 3;
					break;
			}
			correct = false;
			while(!correct){
				System.out.print("Gib eine Zahl zwischen 1 und 11 ein, mit 1 schießt du weit nach links, mit 5 geradeaus und mit 11 weit nach links.\n");
				userInput = Integer.parseInt(in.readLine());
				if((userInput < 12) && (userInput > 0)){
					correct = true;
				} else {
					System.out.print("Falsche Eingabe!\n");
				}
			}
			if((wind == 1) && (wind + userInput < 4)){
				System.out.print("Treffer!\n");
				point++;
			} else if ((wind == 2) && (wind + userInput < 7) && (wind + userInput > 4)){
				System.out.print("Treffer!\n");
				point++;
			} else if ((wind == 3) && (userInput == 5)){
				System.out.print("Treffer!\n");
				point++;
			} else if ((wind == 4) && (wind + userInput < 13) && (wind + userInput > 9)){
				System.out.print("Treffer!\n");
				point++;
			} else if ((wind == 5) && (wind + userInput < 17) && (wind + userInput > 14)){
				System.out.print("Treffer!\n");
				point++;
			} else {
				System.out.print("Du hast das Ziel leider verfehlt.");
			}
		}
	}

}
