import java.io.*;

/**  This will be the character sheet for the protagonist
 *  It is created when you start a new game and will save and
 *  load your progress to and from a TSV file
 */
public class Lilith {
    File path = new File("../Lilith.tsv");
    int lastChapterPlayed = 1;

    // basic parameters
    private int lvl = 1;
    private int health = 50; //TODO must be balanced later
    private int maxHealth = 50;
    private int dext = 30; //TODO must be balanced later
    private int maxDext = 30;
    private int xp = 0;

    // preferences
    private String weapon = "";
    private int bowLvl = 1;
    private int bowXP = 0;
    private int swordLvl = 1;
    private int swordXP = 0;

    public Lilith(File filename) throws IOException {
        setPath(filename);
        // create given file, do not append if it already exists (overwrite)
        FileWriter characterSheet = new FileWriter(filename, false);
    }

    public File getPath() {
        return path;
    }

    public void setPath(File path) {
        this.path = path;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getDext() {
        return dext;
    }

    public void setDext(int dext) {
        this.dext = dext;
    }

    public int getMaxDext() {
        return maxDext;
    }

    public void setMaxDext(int maxDext) {
        this.maxDext = maxDext;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public int getBowLvl() {
        return bowLvl;
    }

    public void setBowLvl(int bowLvl) {
        this.bowLvl = bowLvl;
    }

    public int getBowXP() {
        return bowXP;
    }

    public void setBowXP(int bowXP) {
        this.bowXP = bowXP;
    }

    public int getSwordLvl() {
        return swordLvl;
    }

    public void setSwordLvl(int swordLvl) {
        this.swordLvl = swordLvl;
    }

    public int getSwordXP() {
        return swordXP;
    }

    public void setSwordXP(int swordLvL) {
        this.swordXP = swordLvL;
    }

    public int getLastChapterPlayed() {
        return lastChapterPlayed;
    }

    public void setLastChapterPlayed(int lastChapterPlayed) {
        this.lastChapterPlayed = lastChapterPlayed;
    }

    /** favourite weapon determines leveling
     * empty -> both level the same speed
     * bow -> 25% more XP for bow and less for sword
     * sword -> the other way around
     */

    public void levelBow(int bowXP) {
        if(weapon.equals("bow")) {
            this.bowXP += (bowXP * 25)/100;
        } else if(weapon.equals("sword")) {
            this.bowXP -= (bowXP * 25)/100;
        }

        this.bowXP += bowXP;
        if(this.bowXP >= 100) {
            this.bowLvl++;
            this.bowXP -= 100;
        }
    }

    public void levelSword(int swordXP) {
        if(weapon.equals("bow")) {
            this.swordXP -= (swordXP * 25)/100;
        } else if(weapon.equals("sword")) {
            this.swordXP += (swordXP * 25)/100;
        }

        this.swordXP += swordXP;
        if(this.swordXP >= 100) {
            this.swordLvl++;
            this.swordXP -= 100;
        }
    }

    public void levelCharacter(int XP) {
        this.xp += XP;
        if (this.xp >= 100) {
            System.out.println("Höheres Level erreicht!\nGesundheit und Ausdauer erhöhen sich!");
            this.lvl++;
            this.xp -= 100;
            this.maxHealth += (this.maxHealth*10)/100;
            this.health = this.maxHealth;
            this.maxDext += (this.maxDext*10)/100;
            this.dext = this.maxDext;
        }
    }

    public void toTSV() throws IOException {
        StringBuilder tsvWriter = new StringBuilder();

        tsvWriter.append("Level");
        tsvWriter.append('\t');
        tsvWriter.append(getLvl());
        tsvWriter.append('\n');

        tsvWriter.append("Health");
        tsvWriter.append('\t');
        tsvWriter.append(getHealth());
        tsvWriter.append('\n');

        tsvWriter.append("maxHealth");
        tsvWriter.append('\t');
        tsvWriter.append(getMaxHealth());
        tsvWriter.append('\n');

        tsvWriter.append("dext");
        tsvWriter.append('\t');
        tsvWriter.append(getDext());
        tsvWriter.append('\n');

        tsvWriter.append("maxDext");
        tsvWriter.append('\t');
        tsvWriter.append(getMaxDext());
        tsvWriter.append('\n');

        tsvWriter.append("XP");
        tsvWriter.append('\t');
        tsvWriter.append(getXp());
        tsvWriter.append('\n');

        tsvWriter.append("Weapon");
        tsvWriter.append('\t');
        tsvWriter.append(getWeapon());
        tsvWriter.append('\n');

        tsvWriter.append("Bow level");
        tsvWriter.append('\t');
        tsvWriter.append(getBowLvl());
        tsvWriter.append('\n');

        tsvWriter.append("Bow XP");
        tsvWriter.append('\t');
        tsvWriter.append(getBowXP());
        tsvWriter.append('\n');

        tsvWriter.append("Sword level");
        tsvWriter.append('\t');
        tsvWriter.append(getSwordLvl());
        tsvWriter.append('\n');

        tsvWriter.append("Sword XP");
        tsvWriter.append('\t');
        tsvWriter.append(getSwordXP());
        tsvWriter.append('\n');

        tsvWriter.append("Chapter");
        tsvWriter.append('\t');
        tsvWriter.append(getLastChapterPlayed());
        tsvWriter.append('\n');
    }

    public void fromTSV() throws IOException {
        BufferedReader tsvReader = new BufferedReader(new FileReader("../Lilith.tsv"));
        String row;
        String[] lilith = new String[12];

        for (int i=0; i<12; i++) {
            row = tsvReader.readLine(); //read a single line from the TSV
            String[] data = row.split("\t");
            lilith[i] = data[1]; //write the value for given key in the array
        }

        setLvl(Integer.parseInt(lilith[0]));
        setHealth(Integer.parseInt(lilith[1]));
        setMaxHealth(Integer.parseInt(lilith[2]));
        setDext(Integer.parseInt(lilith[3]));
        setMaxDext(Integer.parseInt(lilith[4]));
        setXp(Integer.parseInt(lilith[5]));
        setWeapon(lilith[6]);
        setBowLvl(Integer.parseInt(lilith[7]));
        setBowXP(Integer.parseInt(lilith[8]));
        setSwordLvl(Integer.parseInt(lilith[9]));
        setSwordXP(Integer.parseInt(lilith[10]));
        setLastChapterPlayed(Integer.parseInt(lilith[11]));

        tsvReader.close();
    }
}
