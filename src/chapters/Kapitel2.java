package chapters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Kapitel2 {
	private static boolean status = true;
	private static int choice;
	private static BufferedReader in;
	private static boolean correct;

	public static boolean isStatus() {
		return status;
	}

	public static void setStatus(boolean status) {
		Kapitel2.status = status;
	}

	public static int getChoice() {
		return choice;
	}

	public static void setChoice(int choice) {
		Kapitel2.choice = choice;
	}

	public static boolean isCorrect() {
		return correct;
	}

	public static void setCorrect(boolean correct) {
		Kapitel2.correct = correct;
	}
	
	public static void aNormalDay() throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("\nKAPITEL ZWEI\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Aufstehen Lilith!>> die Stimme deiner Zimmergenossin reißt dich unsanft aus dem Schlaf.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Du hattest von der gestrigen Begegnung in der Bibliothek geträumt und als Nuy die Botschaft übermittelt hatte, hattest du einen Schatten gesehen. Einen Schatten und zwei große grüne Augen mit schlitzförmigen Pupillen, wie Katzenaugen.\n"
				+ "<<Jetzt steh schon auf, wir müssen in zehn Minuten auf dem Trainingsplatz sein.>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Du streckst dich verschlafen, dann schwingst du deine Beine vom Bett und suchst deine Trainingsklamotten zusammen.\n"
				+ "<<Fang!>> Du siehst etwas - deine Jacke - auf dich zufliegen.\n");
		correct = false;
		while(!correct){
			System.out.print("Schreibe eine ganze Zahl zwischen 0 und 10.\n");
			//TODO add timer, so you fail if you're too slow
			choice = Integer.parseInt(in.readLine());
			if((choice < 0) || (choice > 10)){
				System.out.print("Falsche Eingabe!");
			} else if (choice%2 == 0){ //TODO make decision based on a random number to make it impossible to figure the system out
				System.out.print("Du schließt deine Hand einen kleinen Moment zu früh und die Jacke fällt auf den Boden. Mit einem gemurmelten <<Dankeschön>> hebst du sie wieder auf.\n");
				correct = true;
			} else {
				System.out.print("Du fängst die Jacke geschickt auf, ziehst sie an und bedankst dich.\n");
				correct = true;
			}
		}
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Wo warst du gestern eigentlich noch so lange?>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		correct = false;
		while(!correct){
			System.out.print("Willst Du ihr von deiner Begegnung mit Nuy erzählen? 1. Ja oder 2. Nein\n");
			choice = Integer.parseInt(in.readLine());
			if(choice == 1){
				System.out.print("<<Ich war in der Bibliothek,...>> wenn du ihr von der Begegnung erzählst, dann läufst du in Gefahr, dass ein Meister davon erfährt. Da du Nuy nicht sofort dem Großmeister gemeldet hast, kann das große Probleme für dich bedeuten. <<... Jack hat mich irgendwann abgeholt und mich gezwungen, etwas zu essen.>>\n");
				correct = true;
			} else if(choice == 2){
				System.out.print("<<Ich war in der Bibliothek und dann hat Jack mich noch gezwungen, etwas zu essen.>>\n");
				correct = true;
			} else {
				System.out.print("Falsche Eingabe!\n");
			}
		}
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Eines Tages werdet ihr noch heiraten. Eine schlechte Partie ist er definitiv nicht. Er sieht gut aus und stammt aus einer reichen und angesehenen Familie.>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Ach sei doch still Clara. Du weißt doch ganz genau, dass er für mich eher wie ein Bruder ist. Er ist für mich da seit ich vor neun Jahren hierhergekommen bin. Jetzt komm, wir verspäten uns noch.>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Wer ist erst vor ein paar Minuten aufgewacht?>> Lachend machen wir uns auf den Weg und kommen zum Glück weder zu spät noch als letzte an.\n");
		TimeUnit.MILLISECONDS.sleep(200);
		System.out.print("<<Lilith, kommst du bitte nach dem Training zu mir.>> Der Meister wirft dir einen kurzen, prüfenden Blick zu.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Jack räuspert sich, als wolle er etwas einwerfen. <<Oder komm lieber zu mir nachdem du etwas gegessen hast.>> Diesmal ist der Blick des Meisters etwas einsichtiger.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Du bist verwirrt. Was will er von dir? Hat es etwas mit der Begegnung der letzten Nacht zu tun?\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Das Training ist sehr anstrengend, du bist müde und mit den Gedanken weit  weg. Mehrfach muss Jack dich in die Gegenwart zurück holen.\n"
				+ "\n");
		TimeUnit.MILLISECONDS.sleep(200);
		System.out.print("<<Was ist los Lil?>> Jack hat dich nach dem Training zur Seite genommen und ihr wartet, bis die Anderen ein Stück voraus gegangen sind.\n");
		correct = false;
		TimeUnit.MILLISECONDS.sleep(100);
		while(!correct){
			System.out.print("Was willst Du Jack antworten? 1. Die Wahrheit oder 2. deinen Schlafmangel für alles verantwortlich machen.\n");
			choice = Integer.parseInt(in.readLine());
			if (choice == 1){
				System.out.println("Du erzählst Jack von deiner Begegnung mit Nuy, versuchst dich an jedes ihrer Worte zu erinnern. Auch deinen Traum lässt du nicht unerwähnt.\n");
				TimeUnit.MILLISECONDS.sleep(100);
				System.out.print("<<Das ist gefährlich, warum hast du mir nicht gestern schon davon erzählt? Wir müssen sofort zum Großmeister!>>\n");
				TimeUnit.MILLISECONDS.sleep(100);
				System.out.print("Mit einem unguten Gefühl folgst du Jack zu den Räumen des Großmeisters der Drachenjäger. Auch ihm erzählst du die ganze Geschichte.\n")
				TimeUnit.MILLISECONDS.sleep(100);
				System.out.print("<<Ich halte es für das Beste, wenn du nicht mit in den Norden gehst. Wenn diese Nuy dich einmal gefunden hat, dann wird sie dich ohne Probleme noch ein weiteres mal finden. Die Gefahr für dich ist zu groß.\n"
				+"Außerdem hättest du den Vorfall augenblicklich melden sollen. Wir können nur Drachenjäger mit auf eine Expedition nehmen, denen wir voll und ganz vertrauen können.>>\n");
				TimeUnit.MILLISECONDS.sleep(100);
				System.out.print("Es war ein Fehler Jack und damit dem Großmeister von der Begegnung zu erzählen.");
				return;
			} else if(choice == 2){
				correct = true;
			} else {
				System.out.print("Falsche Eingabe!\n");
			}
		}
		System.out.print("<<Ich bin müde und frage mich was der Meister von mir will. Ich habe doch gegen keine Regel verstoßen oder?>> Es fühlt sich nicht gut an Jack zu belügen, aber du bist dir sicher dass es besser so ist.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Du musst noch  deinen Eid ablegen bevorwir aufbrechen.>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Warum müssen wir eigentlich immer vor dem Essen trainieren? Ich habe Hunger!>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Jack lacht. <<Und das kommt von dir? Wer verpasst in letzter Zeit ständig Mahlzeiten?>>\n");
		TimeUnit.MILLISECONDS.sleep(200);
		System.out.print("Nach dem Frühstück machen Lilith und Jack sich auf zu dem Meister, sie finden ihn auf dem Trainingsplatz.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Da seid ihr ja, gut dass du auch hier bist jack, du kannst gleich mitkommen.>> Der Meister lächelt sie freundlich an.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Nach ein paar Metern beginnt der Meister wieder zu sprechen. <<Obwohl du eigentlich noch zu jung bist, haben wir beschlossen, dass du Morgen deinen Eid vom dem König ablegst.\n"
		+ "Ansonsten müssten wir dich hier lassen umd um ehrlich zu sein wäre das ein Verlust für die Gruppe.\n");
		TimeUnit.MILLISECONDS.sleep(50);
		System.out.print("Jack, da du ihr Trainingspartner und früherer Mentor bis, wirst du für Lilith bürgen. Außerdem wird der König euch zu Kampfpartnern machen.\n"
		+ "Ich hoffe ihr wisst was das für eine Ehre ist. Sucht nach dem Mittagessen bitte den Großmeister auf. Er möchte noch einmal mit euch reden.>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Jack und Du nicken zustimmend. <<Jetzt geht wieder trainieren.>> Mit diesen Worten entlässt der Meister euch.\n");
		TimeUnit.MILLISECONDS.sleep(150);
		System.out.println("<<Und wer hatte wieder Recht?>> Jack lächelt triumphierend, während ihr zu der Waffenkammer für Trainingswaffen geht.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Die Bögen dort sind nicht schlecht, die Pfeile allerdings ohne Metallspitze. Drachenpfeile sind dort ansich nicht zu finden.\n"
		+ "Die Schwerter sind alle stumpf und ohne Spitze, ebenso alle anderen Stichwaffen. Diese Waffen sind nichts im Vergleich zu richtigen Drachenjägerwaffen.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Du natürlich.>>\n");
		correct = false;
		TimeUnit.MILLISECONDS.sleep(100);
		while(!correct){
			System.out.print("<<Was wollen wir trainieren?>> fragt Jack.\n"
			+ "1. Schwert oder 2. Pfeil und Bogen");
			choice = Integer.parseInt(in.readLine());
			if(choice == 1){
				//swordplay missing
				correct = true;
			} else if(choice == 2){
				Archery train = new Archery();
				train.staticTraining();
				correct = true;
			} else {
				System.out.println("Falsche Eingabe!\n");
			}
		}
		
	}
}
