package chapters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class Kapitel1 {
	private static boolean status = true;
	private static int choice;
	private static BufferedReader in;
	private static boolean correct;

	public static boolean isStatus() {
		return status;
	}

	public static void setStatus(boolean status) {
		Kapitel1.status = status;
	}

	public static int getChoice() {
		return choice;
	}

	public static void setChoice(int choice) {
		Kapitel1.choice = choice;
	}

	public static boolean isCorrect() {
		return correct;
	}

	public static void setCorrect(boolean correct) {
		Kapitel1.correct = correct;
	}
	
	public static void beginning() throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Willkommen! Bevor wir anfangen, ein paar allgemeine Informationen: \n"
				+ "Die Person die Du spielst heißt Lilith, sie ist ein Mitglied des Drachenjägerordens.\n"
				+ "Weitere Informationen über sie erfährst Du im Laufe des Spiels.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Im Spiel wirst Du die Möglichkeit haben, zwischen verschiedenen Antwortmöglichkeiten zu wählen.\n"
				+ "Bitte hier immer die Nummer der gewünschten Möglichkeit eingeben.\n"
				+ "Viel Spaß!");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("\n \nDu befindest dich in einer Bibliothek, es ist spät am Abend, deshalb erhellt nur das\n"
				+ " Licht einer Kerze deine Umgebung.");
		correct = false;
		while(!correct) {
			System.out.print("\nWillst Du: 1. Dich umsehen oder 2. In dem Buch in Deiner Hand lesen?\n");
			choice = Integer.parseInt(in.readLine());
			if(choice == 1) {
				System.out.print("\nDu sitzt auf einem Sessel, hinter Dir ist ein Fenster, doch Du siehst lediglich Deine\n"
						+ " eigene Spiegelung im schwachen Kerzenlicht. Vor Dir erstecken sich lange Regalreihen voller\n"
						+ " Büchern, das Licht Deiner Kerze verliert sich schnell zwischen ihnen.\n");
				correct = true;
			} else if(choice != 2) {
				System.out.print("\nFalsche Eingabe!");
			} else {
				correct = true;
			}
		}
		correct = false;
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Du wendest Dich Deinem Buch zu und ließt die aufgeschlagene Seite:\n"
				+ "\nDie Drachen waren häufig auf den Wiesen nahe des großen Walds zu sehen, sie kamen aus dem Diamant-Gebirge \n"
				+ "und nutzen den großen Wald und die Wiesen als Jagdgebiete. Seit dem Zeitalter des ersten Königs der Rubinfamilie\n"
				+ " verschwanden die Drachen nach und nach. Ob es wahr ist oder nicht, ist nicht bewiesen, doch es wird gesagt,\n"
				+ " dass die Rubinfamilie eine Dynastie der Drachenjäger hervorbrachte.\n"
				+ "Drachenjäger existieren seit dem Zeitalter des ersten Königs der Rubinfamilie. Drachenjäger bewahren das Reich\n"
				+ " und sein Volk vo den Drachen.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		while(!correct) {
			System.out.print("Du hörst ein leises Geräusch. Willst Du: 1. Nachsehen woher es kommt oder 2. weiterlesen?\n");
			choice = Integer.parseInt(in.readLine());
			if(choice == 1) {
				nuy();
				correct = true;
			} else if(choice == 2) {
				System.out.print("\nDu ignorierst das Geräusch, ließt noch ein paar Zeilen. Doch dann überkommt Dich eine schwere\n"
						+ " Müdigkeit. Dir fallen die Augen zu und das Buch gleitet langsam aus Deiner Hand und fällt zu Boden.");
				setCorrect(false);				
				return;
			} else {
				System.out.print("\nFalsche Eingabe!");
			}
		}
	}
	
	public static void nuy() throws IOException {
		System.out.print("\nDu stehst leise auf und klappst das Buch zu. Dann nimmst Du die Kerze und machst Dich in die Richtung \n"
				+ "des Geräuschs auf. Die Kerze ist in der Dunkelheit gut sichtbar, aber für einen Drachenjäger sind deine leichten \n"
				+ "Schritte nicht zu überhören. Nachdem Du an einigen Regalreihen vorbei bist, hörst Du jemanden atmen.");
		correct = false;
		while(!correct) {
			System.out.print("\nGreifst Du an deine Seite, wo eigentlich Dein Schwert hängen sollte? 1. Ja oder 2. Nein\n");
			choice = Integer.parseInt(in.readLine());
			TimeUnit.MILLISECONDS.sleep(100);
			if(choice == 1) {
				System.out.print("\nWie kannst Du diese wichtige Regel des Instituts vergessen? Waffen darf man zur besonderen Anlässen \n"
						+ "tragen. Dazu gehören spezielle Trainingseinheiten, Prüfungen oder ein Einsatz.");
				correct = true;
			} else if (choice == 2) {
				System.out.print("\nDu kennst die Regeln gut und weißt, dass Du keine Waffe bei Dir tragen darfst.");
				correct = true;
			} else {
				System.out.print("\nFalsche Eingabe!");
			}
		}
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("\nDu musst noch um ein weiteres Regal gehen, dann stehst Du einen dunklen Schatten. Deine Sinne haben dich \n"
				+ "also nicht getäuscht. Die Person muss Dich bereits bemerkt haben und dreht sich jetzt langsam zu Dir um. \n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Vor dir steht eine junge Frau, kaum älter als Du. Sie trägt einen schwarzen Mantel mit Kapuze, wodurch Du ihr Gesicht\n"
				+ " nicht sehen kannst. Doch einzelne schwarze Stränen schauen unter der Kapuze hervor. Sie kommt dir nicht bekannt vor.\n"
				+ " Kein Mädchen des Ordens hat schwarze Haare. Sie muss aus dem Norden stammen, denn hier im Süden ist diese Haarfarbe \n"
				+ "sehr selten.\n");
		correct = false;
		while(!correct) {
			System.out.print("Willst Du die Person ansprechen? 1. Ja oder 2. Nein\n");
			choice = Integer.parseInt(in.readLine());
			if(choice == 1) {
				correct = true;
			} else if(choice == 2) {
				System.out.print("\nDu siehst sie noch einmal von oben bis unten an. Dann beschließt Du dass Du lieber verschwindest, \n"
						+ "immerhin willst Du keinen Ärger bekommen. Außerdem ist es schon spät und Du willst nur noch schlafen.");
				setCorrect(false);				
				return;
			} else {
				System.out.print("\nFalsche Eingabe!");
			}
		}
		System.out.print("<<Wer bist du?>> fragst Du das Mädchen.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Du willst meinen Namen wissen? Nur wenn ich deinen Namen auch erfahre!>>\n");
		correct = false;
		while(!correct){
			System.out.print("Willst Du der Person deinen richtigen Namen sagen? 1. Nein oder 2. Ja\n");
			choice = Integer.parseInt(in.readLine());
			if(choice == 1){
				falseName();
				setCorrect(false);
				return;
			} else if(choice == 2){
				correct = true;
			} else {
				System.out.print("Falsche Eingabe! \n");
			}
		}
		System.out.print("<<Mein Name ist Lilith, Tochter des Nurien.>> sagst Du.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Ich heiße Nuy.>> mehr gibt die Fremde nicht von sich Preis.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Woher kommst du?>> fragst Du nach kurzem warten.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Aus dem Norden, aber das müsste dir schon aufgefallen sein.>> ihre Stimme hat einen spöttischen Unterton.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Was willst du hier? Nur Mitglieder der Drachenjäger haben Zugang zu dieser Bibliothek.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Wer sagt, dass ich kein Mitglied bin?>>");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Das lässt sich leicht feststellen. Ich bringe dich zum Großmeister, er wird wissen, ob du zu uns gehörst oder nicht.>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Nein bitte nicht!>> Nuys Reaktion lässt keine Zweifel - sie will nicht zum Großmeister gebracht werden.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Es wird aber von mir verlangt. Wenn jemand davon erfährt, dass ich dich nicht gemeldet habe, dann bekomme ich großen Ärger.>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Keiner wird von meiner Anwesenheit erfahren. Ich habe eine Nachricht für dich...>>\n");
		correct = false;
		while(!correct){
			TimeUnit.MILLISECONDS.sleep(100);
			System.out.print("Willst Du 1. deine Pflicht erfüllen, Nuy melden und Ärger entgehen oder 2. ihre Nachricht anhören?\n");
			choice = Integer.parseInt(in.readLine());
			if(choice == 1){
				System.out.print("\n<<Ich muss und werde dich zum Großmeister melden. Ich will meine Zukunft nicht riskieren.>> Mit diesen Worten versuchst Du nach Nuy zu greifen. Doch diese duckt sich unter deinen Armen weg und verschwindet in der Dunkelheit. Kurz noch hörst Du ihre Schritte, doch weißt du, dass du sie nicht mehr finden wirst. Alleine bleibst Du mit deiner Kerze zwischen den Regalen stehen.\n");
				setCorrect(false);		
				return;
			} else if(choice == 2){
				message();
				correct = true;
			} else {
				System.out.print("Falsche Eingabe! \n");
			}
		}
		
	}

	public static void falseName(){
		System.out.print("<<Mein Name ist Clara.>> sagst du.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Kennst Du eine Lilith?>> Fragt das Mädchen.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Ja, aber Du wolltest mir noch deinen Namen nennen>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Mein Name ist unwichtig, Wo finde ich Lilith?>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("In diesem Moment hörst Du eine Tür auf gehen. Das Mädchen sieht sich nervös um, dann verschwindet sie in der Dunkelheit der Bibliothek.\n");
		setCorrect(false);		
		return;
	}

	public static void message(){
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("\n<<Sie haben dich auserwählt, weil sie von deinen Taten gehört haben, von deinem Talent und den Begabungen die dir in die Wiege gelegt wurden. Sie hoffen dass du deine Macht eines Tages kennst und beherrscht, sie sehen in dir ihre einzige Hoffnung.>> Nuy klingt erfürchtig.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Wer sind sie?>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Das werde ich an diesem Ort nicht aussprechen.>>\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Wie lautet die Nachricht?>> fragst du mit wachsender Neugier.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("\n Wir erwarten dich Lilith Tochter des Nurien und du wirst zu uns kommen. Eines Tages bringt Elaisa dich zu uns.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("\nWährend du noch versuchst die Worte zu verstehen, verschwindet Nuy lautlos im Schatten.\n"
				+ "Woher nur kennst du den Namen Elaisa? Noch vor wenigen Tagen war er dir begegnet...\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("Mit einem Schlag fällt es dir ein. Elaisa ist die Schicksalsgöttin einer alten Religion, doch zu welchem Volk gehört sie?\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Zu so später Stunde noch hier?>> eine nur zu bekannte Stimme reißt dich plötzlich aus deinen Gedanken. Vor dir steht dein früherer Mentor, heutiger Trainingspartner und engster Vertrauter.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Musst du mich immer so erschrecken?>> fragst du schnippisch, innerlich bist du aber mehr als froh einen vertrauten Menschen zu sehen, er nimmt dem ganzen Geschehen das Irreale.\n");
		TimeUnit.MILLISECONDS.sleep(100);
		System.out.print("<<Du siehst blass aus, wie ich dich kenne bist du schon seit Stunden hier. Wahrscheinlich hast du auch wieder eine Mahlzeit ausgelassen. Wir holen dir noch schnell etwas in der Küche, dann gehst du schlafen. Du musst fit sein, sonst darfst du vielleicht nicht mit auf die Mission.>> er, Jack, legt freundschaftlich einen Arm um deine Schultern und dirigiert dich aus der Bibliothek.\n");
	}

	setCorrect(true); //Why is there an error??
}
