import chapters.Kapitel1;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;


public class Drachenfeuer {
	static Lilith lilith;

    public static void main(String[] args)throws IOException{
    	int menu = 99;

    	while (menu == 99) {
			menu = menu();
			switch (menu) {
				case 1:
					Kapitel1.beginning();
					//...
				break;
				case 2:
					//Kapitel2. usw.
				break;
				case 42:
					System.out.println("ungültige Eingabe");
					menu = 99;
				break;
				default: return;
			}
		}
    }

    public static int menu() throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("===========================================================");
		System.out.println("=====             DRACHENFEUER - HAUPMENÜ             =====");
		System.out.println("===========================================================");
		System.out.println("\n\n");
    	System.out.println("Neues Spiel (1)");
    	System.out.println("Fortsetzen (2)");
    	System.out.println("Beenden (3)");
    	int menu = Integer.parseInt(in.readLine());

    	switch(menu) {
    		//TODO How do I reference Lilith?
			case 1: // new game
				this.lilith = new Lilith(newGame());
				return 1;

			case 2: // load game
				try {
					File savegames = new File("../savegames");
					File[] saves = savegames.listFiles();
					System.out.println("Spielstand auswählen:");
					for (int i = 0; i < saves.length; i++) {
						System.out.println("(" + (i - 1) + ")" + saves[i]); //TODO mock savegames to test this
					}
					int select = Integer.parseInt(in.readLine());
					this.lilith = new Lilith(saves[select]);
					return lilith.getLastChapterPlayed();
				} catch (NullPointerException e) {
					System.out.println("Keine Spielstände gefunden. Es wird ein neuer Spielstand angelegt.");
					return 99;
				}

			case 3: // exit
				return Integer.MAX_VALUE;
			default: return 42; // restart menu
		}
	}

	public static File newGame() throws IOException {
    	BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Gib einen Namen für deinen Spielstand ein.");
		String filename = in.readLine();

		try {
			File path = new File("../savegames/" + filename + ".tsv");
		} catch (FileAlreadyExistsException) {
			System.out.println("Datei " + filename + " existiert bereits. \n" +
					"Soll sie überschrieben werden? (j/n)");
			switch (in.readLine().toLowerCase()) {
				case "j":
					newGame(filename);
				case "n":
					newGame();
				default: System.out.println("Ungültige Eingabe");
			}
		}
	}

	public static File newGame(String filename) {
    	//force given pathname and overwrite existing file

	}
}
